## Doing
* Przygotować sprawozdanie
* Sprawozdanie powinno zawierać krótki opis zaproponowanego rozwiązania oraz instrukcję obsługi.

## Done
* Aplikacja powinna umożliwiać posortowanie listy produktów po danych produktu (nazwa, cena itd...).
* Do sortowania można wykorzystać np. ComboBox z listą elementów, po których można posortować. 
* Dodatkowo powinna być możliwość filtrowania listy po cenie wpisując cene od i do.
* Produkty powinny być przechowywane jedynie w pamięci przeglądarki.
* Aplikacja powinna działać jedynie po stronie przeglądarki (bez częsci serwerowej).
* Produkty powinny być renderowane na stronie przy użyciu samodzielnie stworzonej dyrektywy. 
* Aplikacja powinna umożliwiać dodawanie, usuwanie i modyfikację produktów z listy
* Przy uruchomieniu aplikacji na liście powinny się znajdować przykładowe produkty.
* Technologie, które należy użyć: AngularJS, Bower, Node.js, Bootstrap
* Biblioteki wykorzystywane w aplikacji powinny być zarządzane przez Bowera (przygotować plik bower.json)
* SKonfigurować AngularJS
* Należy dostarczyć niezbędną konfigurację tak aby aplikacja bez problemu uruchamiała się na Node.js (należy przygotowac plik package.json).
* Komenda „npm install” powinna zainstalować niezbędne pakiety wymagane przez aplikację a komenda „npm start” powinna uruchomić serwer z działającą aplikacją. 
* Aplikacja powinna zawierać listę produktów (np. samochody, kosmetyki). 
* Produkt powinien się składać z obrazka (url do obrazka gdzieś w internecie), nazwy, ceny, opisu produktu oraz innych danych dotyczących produktu (minimum 3). https://www.mockaroo.com/schemas/download
* Dorobić stronę NoExistingPage
* Sprawdzić jakie biblioteki muszą być preinstalowane
* Walidacja formularza
* Sprawdzić czy spełnione są oceniane cechy.

## Instrukcja do setupu
* Zainstalować node w wersji conajmniej 6.0