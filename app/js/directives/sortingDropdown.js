"use strict";

productsViewerApp.directive("sortingDropdown", function() {
  return {
    restrict: 'E',
    templateUrl: 'app/templates/directives/sortingDropdown.html',
    replace: true,
    scope: {
      options: '=',
      val: '='
    },
  };
});