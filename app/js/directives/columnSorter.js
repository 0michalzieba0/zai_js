"use strict";

productsViewerApp.directive("columnSorter", function() {
  return {
    restrict: "E",
    templateUrl: "app/templates/directives/columnSorter.html",
    scope: {
      columnDisplayName: "@columndisplayname",
      columnName: "@columnname",
      config: "="
    }
  };
});
