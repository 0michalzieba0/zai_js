"use strict";

productsViewerApp.directive("addProductBtn", function() {
  return {
    restrict: 'E',
    templateUrl: 'app/templates/directives/addProductBtn.html',
    replace: true
  };
});