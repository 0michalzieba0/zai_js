"use strict";

productsViewerApp.directive("productsList", function() {
  return {
    restrict: 'E',
    templateUrl: 'app/templates/directives/productsList.html',
    replace: true,
    scope: {
      state: '='
    },
    terminal  : true
  };
});
