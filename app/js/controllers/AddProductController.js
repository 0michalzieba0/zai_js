"use strict";

productsViewerApp.controller(
  "AddProductController",
  function AddProductController($scope, $location, ProductsService) {
    $scope.pageName = "Dodawanie produktu";
    $scope.product = null;

    $scope.save = function(product) {
      ProductsService.save(product);
      $location.url("/");
    };
  }
);
