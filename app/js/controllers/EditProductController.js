"use strict";

productsViewerApp.controller(
  "EditProductController",
  function EditProductController($scope, $routeParams, $location, ProductsService) {
    var productId = parseInt($routeParams.productId);
    $scope.pageName = 'Edycja produktu';
    ProductsService.getById(productId).then(function(product){
      $scope.product = product;
    });

    $scope.save = function(product) {
      ProductsService.update(product);
      $location.url('/');
    };
  }
);
