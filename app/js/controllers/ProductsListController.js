"use strict";

productsViewerApp.controller(
  "ProductsListController",
  function ProductsListController($scope, ProductsService) {
    var sortOptions = [
      { value: "id", name: "Sortowanie po: Id" },
      { value: "name", name: "Sortowanie po: Nazwa" },
      { value: "price", name: "Sortowanie po: Cena" },
      { value: "description", name: "Sortowanie po: Opis" },
      { value: "producer", name: "Sortowanie po: Producent" },
      { value: "origin", name: "Sortowanie po: Miejsce pochodzenia" },
      { value: "weight", name: "Sortowanie po: Waga" }
    ];

    $scope.internalState = {
      products: [],
      deleteProduct: function(id) {
        ProductsService.delete(id);
      },
      sortingConfig: {
        sortOptions: sortOptions,
        columnName: "id",
        reverse: false,
        sortBy: function(columnName) {
          $scope.internalState.sortingConfig.columnName = columnName;
          $scope.internalState.sortingConfig.reverse = !$scope.internalState
            .sortingConfig.reverse;
        }
      },
      filterConfig: {
        priceGreaterOrEqualThan: undefined,
        priceLessOrEqualThan: undefined
      }
    };

    ProductsService.getAll().then(function(data) {
      $scope.internalState.products = data;
    });
  }
);
