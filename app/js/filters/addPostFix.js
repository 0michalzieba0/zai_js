"use strict";

productsViewerApp.filter("addPostFix", function() {
  return function(text, postfix) {
    return new String(text) + ' ' + postfix;
  };
});

