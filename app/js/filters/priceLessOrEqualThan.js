"use strict";

productsViewerApp.filter("priceLessOrEqualThan", function() {
  return function(products, number) {
    if (!number || isNaN(number)) {
      return products;
    }

    return products.filter(function(product) {
      return product.price <= number;
    });
  };
});

