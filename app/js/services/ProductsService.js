"use strict";

productsViewerApp.factory("ProductsService", function($http, $q) {
  this.data = [];
  var self = this;

  function getData() {
    if (self.data.length > 0) {
      var deferred = $q.defer();
      deferred.resolve(self.data);
      return deferred.promise;
    } else {
      return $http.get("app/data/products.json").then(function(result) {
        self.data = result.data;
        return self.data;
      });
    }
  }

  return {
    getAll: getData,
    getById(id) {
      return getData().then(function(data) {
        return data.find(function(element) {
          return element.id === id;
        });
      });
    },
    delete(id) {
      for (var i = self.data.length - 1; i >= 0; i--) {
        if (self.data[i].id === id) {
          self.data.splice(i, 1);
        }
      }
    },
    save(product) {
      getData().then(function(data) {
        var ids = data.map(function(product) {
          return product.id;
        });
        self.data.push({
          id: Math.max.apply(Math, ids) + 1,
          image: product.image,
          name: product.name,
          price: product.price,
          description: product.description,
          producer: product.producer,
          origin: product.origin,
          weight: product.weight
        });
      });
    },
    update(product) {
      getData().then(function(data) {
        for (var i = data.length - 1; i >= 0; i--) {
          if (data[i].id === product.id) {
            data[i].image = product.image;
            data[i].name = product.name;
            data[i].price = product.price;
            data[i].description = product.description;
            data[i].producer = product.producer;
            data[i].origin = product.origin;
            data[i].weight = product.weight;
          }
        }
      });
    }
  };
});
