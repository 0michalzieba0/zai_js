"use strict";

var productsViewerApp = angular
  .module("productsViewerApp", ["ngRoute"])
  .config(function($routeProvider) {
    $routeProvider
      .when("/", {
        templateUrl: "app/templates/home.html",
        controller: "ProductsListController"
      })
      .when("/addProduct", {
        templateUrl: "app/templates/editProduct.html",
        controller: "AddProductController"
      })
      .when("/editProduct/:productId", {
        templateUrl: "app/templates/editProduct.html",
        controller: "EditProductController"
      })
      .otherwise({
        templateUrl: "app/templates/pageNotExist.html"
      });
  })
  .config([
    "$locationProvider",
    function($locationProvider) {
      $locationProvider.hashPrefix("");
    }
  ]);
